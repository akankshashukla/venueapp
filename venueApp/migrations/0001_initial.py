# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Listcuisines',
            fields=[
                ('name', models.CharField(max_length=50, serialize=False, primary_key=True, db_column='Name')),
                ('iconclass', models.CharField(max_length=50, db_column='IconClass')),
            ],
            options={
                'db_table': 'ListCuisines',
            },
        ),
        migrations.CreateModel(
            name='Listfacilities',
            fields=[
                ('name', models.CharField(max_length=50, serialize=False, primary_key=True, db_column='Name')),
            ],
            options={
                'db_table': 'ListFacilities',
            },
        ),
        migrations.CreateModel(
            name='Listmediatypes',
            fields=[
                ('mediatype', models.CharField(max_length=50, serialize=False, primary_key=True, db_column='MediaType')),
            ],
            options={
                'db_table': 'ListMediaTypes',
            },
        ),
        migrations.CreateModel(
            name='Listoccasions',
            fields=[
                ('name', models.CharField(max_length=50, serialize=False, primary_key=True, db_column='Name')),
                ('iconclass', models.CharField(max_length=50, db_column='IconClass')),
            ],
            options={
                'db_table': 'ListOccasions',
            },
        ),
        migrations.CreateModel(
            name='Listpaymenttype',
            fields=[
                ('name', models.CharField(max_length=50, serialize=False, primary_key=True, db_column='Name')),
                ('iconclass', models.CharField(max_length=50, db_column='IconClass')),
            ],
            options={
                'db_table': 'ListPaymentType',
            },
        ),
        migrations.CreateModel(
            name='Listspacetypes',
            fields=[
                ('name', models.CharField(max_length=50, serialize=False, primary_key=True, db_column='Name')),
                ('iconclass', models.CharField(max_length=50, db_column='IconClass')),
            ],
            options={
                'db_table': 'ListSpaceTypes',
            },
        ),
        migrations.CreateModel(
            name='Searchtoken',
            fields=[
                ('token', models.CharField(max_length=100, serialize=False, primary_key=True, db_column='Token')),
            ],
            options={
                'db_table': 'SearchToken',
            },
        ),
        migrations.CreateModel(
            name='Venue',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, db_column='Id')),
                ('name', models.CharField(max_length=200, db_column='Name')),
                ('description', models.TextField(null=True, db_column='Description', blank=True)),
                ('address', models.CharField(max_length=200, db_column='Address')),
                ('locality', models.CharField(max_length=50, null=True, db_column='Locality', blank=True)),
                ('city', models.CharField(max_length=50, null=True, db_column='City', blank=True)),
                ('state', models.CharField(max_length=50, null=True, db_column='State', blank=True)),
                ('pincode', models.CharField(max_length=50, null=True, db_column='Pincode', blank=True)),
                ('mobile1', models.CharField(max_length=20, null=True, db_column='Mobile1', blank=True)),
                ('mobile2', models.CharField(max_length=20, null=True, db_column='Mobile2', blank=True)),
                ('landline', models.CharField(max_length=20, null=True, db_column='Landline', blank=True)),
                ('fax', models.CharField(max_length=20, null=True, db_column='Fax', blank=True)),
                ('latitude', models.FloatField(null=True, db_column='Latitude', blank=True)),
                ('longitude', models.FloatField(null=True, db_column='Longitude', blank=True)),
                ('url', models.CharField(max_length=200, null=True, db_column='URL', blank=True)),
                ('emailaddress', models.CharField(max_length=50, null=True, db_column='EmailAddress', blank=True)),
                ('coverphotoid', models.IntegerField(null=True, db_column='CoverPhotoId', blank=True)),
                ('operatingtime', models.CharField(max_length=50, null=True, db_column='OperatingTime', blank=True)),
                ('totalviews', models.IntegerField(null=True, db_column='TotalViews', blank=True)),
                ('totalenquiries', models.IntegerField(null=True, db_column='TotalEnquiries', blank=True)),
                ('minseatingcapacity', models.IntegerField(db_column='MinSeatingCapacity')),
                ('maxseatingcapacity', models.IntegerField(db_column='MaxSeatingCapacity')),
                ('costperperson', models.IntegerField(db_column='CostPerPerson')),
                ('isactive', models.IntegerField(null=True, db_column='IsActive', blank=True)),
                ('isinhousekitchen', models.IntegerField(null=True, db_column='IsInHouseKitchen', blank=True)),
            ],
            options={
                'db_table': 'Venue',
            },
        ),
        migrations.CreateModel(
            name='Venuemedia',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, db_column='Id')),
                ('venueid', models.IntegerField(db_column='VenueId')),
                ('url', models.CharField(max_length=200, db_column='URL')),
                ('mediatype', models.CharField(max_length=50, db_column='MediaType')),
                ('vieworder', models.IntegerField(null=True, db_column='ViewOrder', blank=True)),
                ('isactive', models.IntegerField(null=True, db_column='IsActive', blank=True)),
            ],
            options={
                'db_table': 'VenueMedia',
            },
        ),
        migrations.CreateModel(
            name='Venuetocuisinemapper',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, db_column='Id')),
                ('venueid', models.IntegerField(db_column='VenueId')),
                ('cuisinename', models.CharField(max_length=50, db_column='CuisineName')),
                ('isactive', models.IntegerField(null=True, db_column='IsActive', blank=True)),
            ],
            options={
                'db_table': 'VenueToCuisineMapper',
            },
        ),
        migrations.CreateModel(
            name='Venuetofacilitymapper',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, db_column='Id')),
                ('venueid', models.IntegerField(db_column='VenueId')),
                ('facilityname', models.CharField(max_length=50, db_column='FacilityName')),
                ('isactive', models.IntegerField(null=True, db_column='IsActive', blank=True)),
            ],
            options={
                'db_table': 'VenueToFacilityMapper',
            },
        ),
        migrations.CreateModel(
            name='Venuetooccasionmapper',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, db_column='Id')),
                ('venueid', models.IntegerField(db_column='VenueId')),
                ('occasionname', models.CharField(max_length=50, db_column='OccasionName')),
                ('isactive', models.IntegerField(null=True, db_column='IsActive', blank=True)),
            ],
            options={
                'db_table': 'VenueToOccasionMapper',
            },
        ),
        migrations.CreateModel(
            name='Venuetopaymenttypemapper',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, db_column='Id')),
                ('venueid', models.IntegerField(db_column='VenueId')),
                ('paymenttype', models.CharField(max_length=50, db_column='PaymentType')),
                ('isactive', models.IntegerField(null=True, db_column='IsActive', blank=True)),
            ],
            options={
                'db_table': 'VenueToPaymentTypeMapper',
            },
        ),
        migrations.CreateModel(
            name='Venuetospacetypemapper',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, db_column='Id')),
                ('venueid', models.IntegerField(db_column='VenueId')),
                ('spacename', models.CharField(max_length=200, null=True, db_column='SpaceName', blank=True)),
                ('spacetype', models.CharField(max_length=50, db_column='SpaceType')),
                ('minseatingcapacity', models.IntegerField(null=True, db_column='MinSeatingCapacity', blank=True)),
                ('maxseatingcapacity', models.IntegerField(null=True, db_column='MaxSeatingCapacity', blank=True)),
                ('isactive', models.IntegerField(null=True, db_column='IsActive', blank=True)),
            ],
            options={
                'db_table': 'VenueToSpaceTypeMapper',
            },
        ),
    ]
