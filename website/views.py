from django.shortcuts import render
from django.shortcuts import render_to_response
from django.template import RequestContext
from forms import searchForm
from django.http import HttpResponse

# Create your views here.
def home_page(request):
    form = searchForm()
    return render_to_response('website/home.html', {'form':form},
                              context_instance=RequestContext(request))
    

def search(request):
    if request.method == 'POST':
        form = searchForm(request.POST)
        if form.is_valid():
            type = form.cleaned_data['venueType']
            loc = form.cleaned_data['venueLocation']            
            # return HttpResponse('thanks')
