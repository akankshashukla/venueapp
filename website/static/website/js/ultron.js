﻿$(document).ready(function () {
    reAlignStrips();

	function reAlignStrips()
	{
	    var windowWidth = $(window).width();
	    $("#movingStrip1").width(2 * windowWidth);
	    $("#movingStrip2").width(2 * windowWidth);
	    $("#movingStrip1").css('margin-left', -windowWidth);
	    $("#movingStrip2").css('margin-left', -windowWidth);
	}

	$(window).resize(function () {
	    reAlignStrips();
	});
});