
from django.conf.urls import url

from . import views

urlpatterns = [
    url(r'^$', views.home_page, name='homepage'),
    url(r'search/', views.search, name='search'),
]

