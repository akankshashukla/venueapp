# This is an auto-generated Django model module.
# You'll have to do the following manually to clean this up:
#   * Rearrange models' order
#   * Make sure each model has one field with primary_key=True
#   * Remove `managed = False` lines if you wish to allow Django to create, modify, and delete the table
# Feel free to rename the models, but don't rename db_table values or field names.
#
# Also note: You'll have to insert the output of 'django-admin sqlcustom [app_label]'
# into your database.
from __future__ import unicode_literals

from django.db import models


class Listcuisines(models.Model):
    name = models.CharField(db_column='Name', primary_key=True, max_length=50)  # Field name made lowercase.
    iconclass = models.CharField(db_column='IconClass', max_length=50)  # Field name made lowercase.

    class Meta:
        db_table = 'ListCuisines'


class Listfacilities(models.Model):
    name = models.CharField(db_column='Name', primary_key=True, max_length=50)  # Field name made lowercase.

    class Meta:
        db_table = 'ListFacilities'


class Listmediatypes(models.Model):
    mediatype = models.CharField(db_column='MediaType', primary_key=True, max_length=50)  # Field name made lowercase.

    class Meta:
        db_table = 'ListMediaTypes'


class Listoccasions(models.Model):
    name = models.CharField(db_column='Name', primary_key=True, max_length=50)  # Field name made lowercase.
    iconclass = models.CharField(db_column='IconClass', max_length=50)  # Field name made lowercase.

    class Meta:
        db_table = 'ListOccasions'


class Listpaymenttype(models.Model):
    name = models.CharField(db_column='Name', primary_key=True, max_length=50)  # Field name made lowercase.
    iconclass = models.CharField(db_column='IconClass', max_length=50)  # Field name made lowercase.

    class Meta:
        db_table = 'ListPaymentType'


class Listspacetypes(models.Model):
    name = models.CharField(db_column='Name', primary_key=True, max_length=50)  # Field name made lowercase.
    iconclass = models.CharField(db_column='IconClass', max_length=50)  # Field name made lowercase.

    class Meta:
        db_table = 'ListSpaceTypes'


class Searchtoken(models.Model):
    token = models.CharField(db_column='Token', primary_key=True, max_length=100)  # Field name made lowercase.

    class Meta:
        db_table = 'SearchToken'


class Venue(models.Model):
    name = models.CharField(db_column='Name', max_length=200)  # Field name made lowercase.
    description = models.TextField(db_column='Description', blank=True, null=True)  # Field name made lowercase.
    address = models.CharField(db_column='Address', max_length=200)  # Field name made lowercase.
    locality = models.CharField(db_column='Locality', max_length=50, blank=True, null=True)  # Field name made lowercase.
    city = models.CharField(db_column='City', max_length=50, blank=True, null=True)  # Field name made lowercase.
    state = models.CharField(db_column='State', max_length=50, blank=True, null=True)  # Field name made lowercase.
    pincode = models.CharField(db_column='Pincode', max_length=50, blank=True, null=True)  # Field name made lowercase.
    mobile1 = models.CharField(db_column='Mobile1', max_length=20, blank=True, null=True)  # Field name made lowercase.
    mobile2 = models.CharField(db_column='Mobile2', max_length=20, blank=True, null=True)  # Field name made lowercase.
    landline = models.CharField(db_column='Landline', max_length=20, blank=True, null=True)  # Field name made lowercase.
    fax = models.CharField(db_column='Fax', max_length=20, blank=True, null=True)  # Field name made lowercase.
    latitude = models.FloatField(db_column='Latitude', blank=True, null=True)  # Field name made lowercase.
    longitude = models.FloatField(db_column='Longitude', blank=True, null=True)  # Field name made lowercase.
    url = models.CharField(db_column='URL', max_length=200, blank=True, null=True)  # Field name made lowercase.
    emailaddress = models.CharField(db_column='EmailAddress', max_length=50, blank=True, null=True)  # Field name made lowercase.
    coverphotoid = models.IntegerField(db_column='CoverPhotoId', blank=True, null=True)  # Field name made lowercase.
    operatingtime = models.CharField(db_column='OperatingTime', max_length=50, blank=True, null=True)  # Field name made lowercase.
    totalviews = models.IntegerField(db_column='TotalViews', blank=True, null=True)  # Field name made lowercase.
    totalenquiries = models.IntegerField(db_column='TotalEnquiries', blank=True, null=True)  # Field name made lowercase.
    minseatingcapacity = models.IntegerField(db_column='MinSeatingCapacity')  # Field name made lowercase.
    maxseatingcapacity = models.IntegerField(db_column='MaxSeatingCapacity')  # Field name made lowercase.
    costperperson = models.IntegerField(db_column='CostPerPerson')  # Field name made lowercase.
    isactive = models.IntegerField(db_column='IsActive', blank=True, null=True)  # Field name made lowercase.
    isinhousekitchen = models.IntegerField(db_column='IsInHouseKitchen', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'Venue'


class Venuemedia(models.Model):
    venueid = models.IntegerField(db_column='VenueId')  # Field name made lowercase.
    url = models.CharField(db_column='URL', max_length=200)  # Field name made lowercase.
    mediatype = models.CharField(db_column='MediaType', max_length=50)  # Field name made lowercase.
    vieworder = models.IntegerField(db_column='ViewOrder', blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='IsActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'VenueMedia'


class Venuetocuisinemapper(models.Model):
    venueid = models.IntegerField(db_column='VenueId')  # Field name made lowercase.
    cuisinename = models.CharField(db_column='CuisineName', max_length=50)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='IsActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'VenueToCuisineMapper'


class Venuetofacilitymapper(models.Model):
    venueid = models.IntegerField(db_column='VenueId')  # Field name made lowercase.
    facilityname = models.CharField(db_column='FacilityName', max_length=50)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='IsActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'VenueToFacilityMapper'


class Venuetooccasionmapper(models.Model):
    venue = models.IntegerField(db_column='VenueId')  # Field name made lowercase.
    occasionname = models.CharField(db_column='OccasionName', max_length=50)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='IsActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'VenueToOccasionMapper'


class Venuetopaymenttypemapper(models.Model):
    venue = models.IntegerField(db_column='VenueId')  # Field name made lowercase.
    paymenttype = models.ForeignKey('Listpaymenttype', db_column='PaymentType', max_length=50)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='IsActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'VenueToPaymentTypeMapper'


class Venuetospacetypemapper(models.Model):
    venue = models.ForeignKey('Venue', db_column='VenueId')  # Field name made lowercase.
    spacename = models.CharField(db_column='SpaceName', max_length=200, blank=True, null=True)  # Field name made lowercase.
    spacetypes = models.ForeignKey('Listspacetypes', db_column='SpaceType', max_length=50)  # Field name made lowercase.
    minseatingcapacity = models.IntegerField(db_column='MinSeatingCapacity', blank=True, null=True)  # Field name made lowercase.
    maxseatingcapacity = models.IntegerField(db_column='MaxSeatingCapacity', blank=True, null=True)  # Field name made lowercase.
    isactive = models.IntegerField(db_column='IsActive', blank=True, null=True)  # Field name made lowercase.

    class Meta:
        db_table = 'VenueToSpaceTypeMapper'


