from django import forms
from models import Listoccasions, Listspacetypes

class searchForm(forms.Form):
    venueType = forms.ChoiceField(choices=(),widget=forms.Select(attrs={'class': 'form-control'}))
    venueLocation = forms.ChoiceField(choices=(),widget=forms.Select(attrs={'class': 'form-control'}))
    
    def __init__(self, *args, **kwargs):

        super(searchForm, self).__init__(*args, **kwargs)
        try:
            self.fields['venueType'].choices=((test.name, test.name) for test in Listoccasions.objects.all())
            self.fields['venueLocation'].choices=((test.name, test.name) for test in Listspacetypes.objects.all())
        except:
            pass
